<?php
class Settings {


        // printer type can be ip, device, usb or netbios
  const PRINTER_TYPE          = 'IP',
        PRINTER_HOST          = '192.168.1.50',
        PRINTER_PORT          = '9100',
        PRINTER_NAME          = 'Epson TM-T88IV',
        PRINTER_DEVICE        = '/dev/ttyUSB0', // /dev/usb/lp0


        VPN_HOST              = 'vpn.hyndla.pw',
        VPN_GATEWAY           = '10.8.0.1',
        VPN_PORT              = '80',

        APP_SERVER_HOST       = 'balder.egerstudio.no',
        APP_SERVER_PORT       = '80',


        //PATH TO SYSTEM_LOG
        SYSTEM_LOG            = '/log/errors.log',

        CLIENT_ID             = 'egerstudio-test',
        LICENSE               = ''
        ;

}
