<?php
require_once('include/header.php');
error_reporting(E_ALL);
?>


  <body role="document">

    <?php
    require_once('include/navigation.php');
    ?>

    <div class="container theme-showcase" role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron system-status">
        <img src="assets/img/hyndla_logo_web_small.png" class="img-responsive center-block">
        <p>Hyndla Client fra Eger Studio tilbyr utskriftstjenester lokalt fra nettbasert ordresystem</p>
      </div>

      <?php
      $filesize = filesize(dirname(__FILE__).'/log/errors.log');
      $fileinkb = number_format((float)($filesize/1024), 2, '.', '');
      ?>

      <div class="page-header">
        <h1>System log <small><?=$fileinkb ?>kb</small></h1>
      </div>

      <div class="row">
        <div class="col-md-12">
          <pre><code id="systemlog"></code></pre>
        </div>

      </div>
      <!-- end row -->
      <?php require_once('include/bottom.php'); ?>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="assets/js/bootSystemLog.js"></script>



  </body>
</html>
