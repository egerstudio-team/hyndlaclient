
   function pingHosts()
    {
      $.ajax({
         type: "GET",
         url: "app/process",
         data: "systempoll=1&unit=printer",
         success: function(response){
            $("#printer").html(response);
          },
       });
       $.ajax({
         type: "GET",
         url: "app/process",
         data: "systempoll=1&unit=vpn",
         success: function(response){
            $("#vpn").html(response);
          },
        });
        $.ajax({
          type: "GET",
          url: "app/process",
          data: "systempoll=1&unit=appserver",
          success: function(response){
             $("#appserver").html(response);
           },
         });

    }

    pingHosts();
    var intervalId;
    intervalId = setInterval(pingHosts,5000);

    $('#systemStatusOn').click(function(){
      pingHosts();
      intervalId = setInterval(pingHosts,5000);
    });

    $('#systemStatusOff').click(function(){
      clearInterval(intervalId);
      $('#printer > div.alert > span > i').toggleClass('fa-spin');
      $('#printer > div.alert').addClass('alert-info');
      $('#vpn > div.alert > span > i').toggleClass('fa-spin');
      $('#vpn > div.alert').addClass('alert-info');
      $('#appserver > div.alert > span > i').toggleClass('fa-spin');
      $('#appserver > div.alert').addClass('alert-info');
    });


    $('#printSystemConfig').click(function(){
      $.ajax({
      type: "GET",
      url: "app/services/print",
      data: "print=hyndlaConfig",
      success: function(response){
         $("#appserver").html(response);
       },
       })
     });
