<?php
require_once('../boot.php');

if (isset($_GET['systempoll']) && isset($_GET['unit'])) {

  switch($_GET['unit']){

    case 'printer':
    $printer = new Status;
    $printer->getInfo('printer');
    break;

    case 'vpn':
    $printer = new Status;
    $printer->getInfo('vpn');
    break;

    case 'appserver':
    $printer = new Status;
    $printer->getInfo('appserver');
    break;

  }

}


if (isset($_GET['systemlog'])) {

          function read_last_lines($fp, $num)
            {
            $idx   = 0;

            $lines = array();
            while(($line = fgets($fp)))
            {
              $lines[$idx] = $line;
              $idx = ($idx + 1) % $num;
            }

            $p1 = array_slice($lines,    $idx);
            $p2 = array_slice($lines, 0, $idx);
            $ordered_lines = array_merge($p1, $p2);

            return $ordered_lines;
            }

            // Open the file and read the last 15 lines
            $fp    = fopen(dirname(__FILE__).'/../..'.Settings::SYSTEM_LOG, 'r');
            $lines = array_reverse(read_last_lines($fp, 50));

            fclose($fp);

            foreach($lines as $line) {
              echo($line);
            }
  }
