<?php

// print hyndla config on printer
/* Information for the configuration items to print */

$content = json_decode(urldecode($_POST['content']),true);
$meta = parse_str($_POST['meta'],$metaDec);

$order = $content[0];
$pickUpNo = $order['id'];
$customer = $order['customer'];
$company = $order['company'];
$pickUpTime = $order['pick_up_time'];



include('static/receipt_top.php');
$pickUpTimeFormatted = date("H:i",strtotime($pickUpTime));
/* If this is a copy, print copytext */
		$printer -> setTextSize(2,2);
		$printer -> text("NY TID\n");
		$printer -> text("kl. ".$pickUpTimeFormatted." \n");
		$printer -> feed();
		$printer -> setTextSize(1,1);

$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> text("HENTENUMMER                 HENTETIDSPUNKT\n");
$printer -> setTextSize(2,2);
$printer -> text(str_pad($pickUpNo,10));
$printer -> text(str_pad($pickUpTimeFormatted,11," ",STR_PAD_LEFT)."\n");
$printer -> setTextSize(1,1);
$printer -> setJustification(Escpos::JUSTIFY_RIGHT);
if($order['confirmed'] == 1) {
	$printer -> text("BEKREFTET\n");
} else {
	$printer -> text("UBEKREFTET\n");
}
$printer -> feed();

$printer -> selectPrintMode();
$printer -> setJustification(Escpos::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text("KUNDE\n");
// if company, print that here after the name
if($company) {
	$printer -> setTextSize(2,2);
	if($company['alias']) {
		$printer -> text($company['alias']."\n");
	} else {
		$printer -> text($company['title']."\n");
	}
	$printer -> setTextSize(1,1);
}
$leftCol = 27;
$rightCol = 15;
$left = str_pad($customer['firstname'].' '.$customer['lastname'],$leftCol);
$right = str_pad("T: ".$customer['phone'],$rightCol," ",STR_PAD_LEFT);
$printer -> text($left.$right."\n");
$printer -> setEmphasis(false);
$printer -> feed();

$printer -> text("Bekreftelseskode\n");
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_WIDTH);
$printer -> text($order['customer_code']."\n");
$printer -> selectPrintMode();
$printer -> feed();

include('static/receipt_bottom.php');



$printer -> cut();
$printer -> pulse();

$printer -> close();
hyndlaLogHandler('PRINT : 100','Print template: '.$_GET['print'].' ('.$conn->printerType.' '.$conn->printerHost.':'.$conn->printerPort.')');
