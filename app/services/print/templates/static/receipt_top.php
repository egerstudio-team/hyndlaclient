<?php
/* Print top logo */
if(file_exists(dirname(__FILE__).'/../../../../../assets/img/'.$metaDec['client'].'.png')) {
	$image = dirname(__FILE__).'/../../../../../assets/img/'.$metaDec['client'].'.png';
} else {
	$image = dirname(__FILE__).'/../../../../../assets/img/default.png';
}
$logo = new EscposImage($image);
$printer -> feed();


$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> graphics($logo);

/* Name of shop */
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_WIDTH);
$printer -> text($metaDec['receiptName']."\n");
$printer -> selectPrintMode();
$printer -> text($metaDec['receiptWeb']."\n");
$printer -> feed();
