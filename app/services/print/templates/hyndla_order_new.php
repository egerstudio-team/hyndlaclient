<?php

// print hyndla config on printer
/* Information for the configuration items to print */


$content = json_decode(urldecode($_POST['content']),true);
$meta = parse_str($_POST['meta'],$metaDec);

$order = $content[0];
$pickUpNo = $order['id'];
$pickUpTime = $order['pick_up_time'];
$orderlines = $order['orderlines'];
$customer = $order['customer'];
$company = $order['company'];
$duplicate = $_POST['duplicate'];



hyndlaLogHandler('ORDER : '.$order['id'],'Customer: '.$customer['phone'].' PickUpTime: '.$pickUpTime);

foreach($orderlines as $orderline) {
	$items[] = new item($orderline['product']['name'], $orderline['quantity'], $orderline['price']);
}



require_once('static/receipt_top.php');

/* If this is a copy, print copytext */
if($duplicate == 1){
		$printer -> setEmphasis(true);		
		$printer -> text("DETTE ER EN KOPI\n");
		$printer -> feed();
}

$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> text("HENTENUMMER                 HENTETIDSPUNKT\n");
$printer -> setTextSize(2,2);
$printer -> text(str_pad($pickUpNo,10));
$pickUpTimeFormatted = date("H:i",strtotime($pickUpTime));
$printer -> text(str_pad($pickUpTimeFormatted,11," ",STR_PAD_LEFT)."\n");
$printer -> setTextSize(1,1);
$printer -> setJustification(Escpos::JUSTIFY_RIGHT);
if($order['confirmed'] == 1) {
	$printer -> text("BEKREFTET\n");
} else {
	$printer -> text("UBEKREFTET\n");
}
$printer -> feed();

$printer -> selectPrintMode();
$printer -> setJustification(Escpos::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text("KUNDE\n");
// if company, print that here after the name
if($company) {
	$printer -> setTextSize(2,2);
	if($company['alias']) {
		$printer -> text($company['alias']."\n");
	} else {
		$printer -> text($company['title']."\n");
	}
	$printer -> setTextSize(1,1);
}
$leftCol = 27;
$rightCol = 15;
$left = str_pad($customer['firstname'].' '.$customer['lastname'],$leftCol);
$right = str_pad("T: ".$customer['phone'],$rightCol," ",STR_PAD_LEFT);
$printer -> text($left.$right."\n");
$printer -> setEmphasis(false);
$printer -> text("------------------------------------------\n");
//$printer -> selectPrintMode(Escpos::MODE_DOUBLE_HEIGHT);
$printer -> setEmphasis(true);
foreach($items as $item) {
	$printer -> text($item);
}
$printer -> selectPrintMode();
$printer -> feed(1);

if($order['comment']){
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> text("------------------------------------------\n");
$printer -> setEmphasis(true);
$printer -> text("KOMMENTAR FRA KUNDE\n");
$printer -> setEmphasis(false);
$printer -> text("------------------------------------------\n");
$printer -> setJustification(Escpos::JUSTIFY_LEFT);
$printer -> text($order['comment']."\n");
$printer -> feed(1);
} // do not print comment if not available

if($order['trailing_title']){
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> text("------------------------------------------\n");
$printer -> setTextSize(2,2);
$printer -> setEmphasis(true);
$printer -> text($order['trailing_title']."\n");
$printer -> setEmphasis(false);
$printer -> setTextSize(1,1);
$printer -> text("------------------------------------------\n");
$printer -> setJustification(Escpos::JUSTIFY_LEFT);
$printer -> text($order['trailing_message']."\n");
$printer -> feed(1);
} // do not print comment if not available


$printer -> text("------------------------------------------\n");





$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> setFont(Escpos::FONT_B);
$printer -> text("BESTILLINGEN MOTTATT\n");
$printer -> text(date("d.m.Y H:i",strtotime($order['created_at']))."\n");
$printer -> feed();
$printer -> setFont(Escpos::FONT_A);
$printer -> text("BEKREFTELSESKODE\n");
$printer -> selectPrintMode(Escpos::MODE_DOUBLE_WIDTH);
$printer -> text($order['customer_code']."\n");
$printer -> selectPrintMode();
$printer -> feed();

include('static/receipt_bottom.php');


$printer -> cut();
$printer -> pulse();

$printer -> close();
hyndlaLogHandler('PRINT : 100','Print template: '.$_GET['print'].' ('.$conn->printerType.' '.$conn->printerHost.':'.$conn->printerPort.')');



/* A wrapper to do organise item names & prices into columns */
class item {
	private $name;
	private $price;
	private $noItems;
	private $dollarSign;

	public function __construct($name = '', $noItems = '', $price = '',$dollarSign = false) {
		$this -> name = $name;
		$this -> noItems = $noItems;
		$this -> price = $price;
		$this -> dollarSign = $dollarSign;
	}

	public function __toString() {
		$rightCols = 0;
		$leftCols = 42;
		$left = str_pad($this -> noItems . ' x '. $this -> name, $leftCols) ;
		$quantity = $this -> noItems;
		return "$left\n";
	}
}
