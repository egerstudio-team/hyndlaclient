<?php

// print hyndla config on printer
/* Information for the configuration items to print */

$content = json_decode(urldecode($_POST['content']),true);
$meta = parse_str($_POST['meta'],$metaDec);

$order = $content[0];
$pickUpNo = $order['id'];
$pickUpTime = $order['pick_up_time'];
$orderlines = $order['orderlines'];
$customer = $order['customer'];



include('static/receipt_top.php');
/* If this is a copy, print copytext */
		$printer -> setTextSize(2,2);
		$printer -> text("KANSELLERT\n");
		$printer -> feed();
		$printer -> setTextSize(1,1);

$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> setEmphasis(true);
$printer -> text("HENTENUMMER                 HENTETIDSPUNKT\n");
$printer -> setTextSize(2,2);
$printer -> text(str_pad($pickUpNo,10));
$pickUpTimeFormatted = date("H:i",strtotime($pickUpTime));
$printer -> text(str_pad('XX:XX',11," ",STR_PAD_LEFT)."\n");
$printer -> setTextSize(1,1);
$printer -> setJustification(Escpos::JUSTIFY_RIGHT);

$printer -> feed();

$printer -> selectPrintMode();
$printer -> setJustification(Escpos::JUSTIFY_LEFT);
$printer -> setEmphasis(true);
$printer -> text("KUNDE\n");
$leftCol = 27;
$rightCol = 15;
$left = str_pad($customer['firstname'].' '.$customer['lastname'],$leftCol);
$right = str_pad("T: ".$customer['phone'],$rightCol," ",STR_PAD_LEFT);
$printer -> text($left.$right."\n");
$printer -> setEmphasis(false);
$printer -> feed();

include('static/receipt_bottom.php');



$printer -> cut();
$printer -> pulse();

$printer -> close();
hyndlaLogHandler('PRINT : 100','Print template: '.$_GET['print'].' ('.$conn->printerType.' '.$conn->printerHost.':'.$conn->printerPort.')');
