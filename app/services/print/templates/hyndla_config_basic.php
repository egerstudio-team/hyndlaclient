<?php

// print hyndla config on printer
/* Information for the configuration items to print */
$configItems = array(
  new configItem("Klient ID:", Settings::CLIENT_ID),
  new configItem("Lisens:", Settings::LICENSE),
  new configItem("",""),
	new configItem("VPN-server: ", Settings::VPN_HOST),
	new configItem("VPN-IP: ", Settings::VPN_GATEWAY),
  new configItem(" "," "),
  new configItem("Printertype: ", Settings::PRINTER_TYPE),
  new configItem("Printer-IP: ", Settings::PRINTER_HOST),
  new configItem("Printerport: ", Settings::PRINTER_PORT),
  new configItem("Printernavn: ", Settings::PRINTER_NAME),
  new configItem("Printerenhet: ", Settings::PRINTER_DEVICE),
  new configItem(" "," "),
  new configItem("Programserver: ", Settings::APP_SERVER_HOST),
  new configItem("Programserver port: ", Settings::APP_SERVER_PORT),
  new configItem("", ""),
  new configItem("Loggfil: ", Settings::SYSTEM_LOG)
);


$logo = new EscposImage(dirname(__FILE__).'/../../../../assets/img/system_logo_print.png');

$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> graphics($logo);
$printer -> feed(2);
$printer -> text("------------------------------------------\n"); // 42 tegn
$printer -> text("HYNDLA KLIENT KONFIGURASJON\n");
$printer -> text("------------------------------------------\n");
$printer -> feed();
$printer -> setJustification(Escpos::JUSTIFY_LEFT);

/* Items */
foreach($configItems as $item) {
	$printer -> text($item);
}


$printer -> setFont(Escpos::FONT_B);
$printer -> setJustification(Escpos::JUSTIFY_CENTER);
$printer -> text("--------------------------------------------------------\n");
$printer -> text("Levert av ");
$printer -> setEmphasis(true);
$printer -> text("HYNDLA ");
$printer -> setEmphasis(false);
$printer -> text("fra ");
$printer -> setEmphasis(true);
$printer -> text("EGER STUDIO\n");
$printer -> setEmphasis(false);
$printer -> text("--------------------------------------------------------\n");
$printer -> text("www.egerstudio.no    support@egerstudio.no\n");
$printer -> text("--------------------------------------------------------\n");


$printer -> cut();
$printer -> pulse();

$printer -> close();
hyndlaLogHandler('PRINT : 100','Print template: '.$_GET['print'].' ('.$conn->printerType.' '.$conn->printerHost.':'.$conn->printerPort.')');
