<?php
require_once(dirname(__FILE__).'/../../boot.php');
require_once('escpos.php');


  $conn = new PrinterConnection(Settings::PRINTER_TYPE,Settings::PRINTER_HOST,Settings::PRINTER_PORT,Settings::PRINTER_DEVICE);
  $printer = $conn->printer;

  if(isset($_GET['print'])) {

    switch($_GET['print']){

        case 'hyndlaConfig':
        include('templates/hyndla_config_basic.php');
        break;

        case 'orderNew':
        include('templates/hyndla_order_new.php');
        break;

        case 'orderUpdate':
        include('templates/hyndla_order_update.php');
        break;

        case 'orderDelete':
        include('templates/hyndla_order_delete.php');
        break;


        default:
        $printer -> close();
        hyndlaLogHandler('PRINT : 001','Print initiated ('.Settings::PRINTER_TYPE.':'.Settings::PRINTER_HOST.'), found content, unmatched, aborting : '.$_SERVER['REMOTE_ADDR'].', '.$_SERVER['HTTP_USER_AGENT']);
        break;


    }

  } else {

    hyndlaLogHandler('PRINT : 000','Print initiated ('.Settings::PRINTER_TYPE.':'.Settings::PRINTER_HOST.'), no content, aborting : '.$_SERVER['REMOTE_ADDR'].', '.$_SERVER['HTTP_USER_AGENT']);
    hyndlaLogHandler('DEBUG : 000','If we got content, this is it:'.$_REQUEST['content']);
    hyndlaLogHandler('DEBUG : 000','If we got a command, this is it:'.$_REQUEST['print']);

    $printer -> close();

  }
