<?php
class PrinterUnreachableException extends Exception{};
class ServerUnreachableException extends Exception{};

class Status
{

    public function getInfo($item) {

      switch($item) {

        case 'printer':
        if(strpos(Settings::PRINTER_TYPE,'IP') !== FALSE) {
          return $this->getInfoHost(Settings::PRINTER_HOST,Settings::PRINTER_PORT,Settings::PRINTER_NAME);
        } else {
          return $this->getPrinterInfo();
        }
        break;

        case 'vpn':
        return $this->getInfoHost(Settings::VPN_GATEWAY,Settings::VPN_PORT,Settings::VPN_HOST);
        break;

        case 'appserver':
        return $this->getInfoHost(Settings::APP_SERVER_HOST,Settings::APP_SERVER_PORT);
        break;

      }
    }


    private function pingHost($host,$port) {
        // simple check to see if the printer responds to a ping
        $starttime = microtime(true);
        $file = fsockopen ($host,$port,$errno,$errstr,3);
        $stoptime = microtime(true);
        $status = array('errorStatus','errorMsg','pingMs');


        if(!$file) { // printer is down
          $status['errno'] = $errno;
          $status['errstr'] = $errstr;
        } else {
          fclose($file);
          $pingMs = 0;
          $pingMs = ($stoptime - $starttime) * 1000;
          $status['ping'] = floor($pingMs);
        }
        return($status);
    }



    private function getInfoHost($server,$port,$name = null) {

      $status = $this->pingHost($server,$port);

      try {
        if(!isset($name)) $name = $server;

        if(!isset($status['ping'])) {
          throw new ServerUnreachableException();
        } else {
          echo('<p><strong>'.$name.'</strong> <span class="label label-success pull-right">ONLINE</span></p>');
          echo('<div class="alert alert-success"><strong>Ping @ '.$status['ping'].' ms</strong>');
          echo('<span class="pull-right statusIndicator"><i class="fa fa-cog fa-spin"></i>&nbsp;</span></div>');
        }
      }
      catch(ServerUnreachableException $ex)
      {

        echo('<p><strong>'.$name.'</strong> <span class="label label-danger pull-right">OFFLINE</span></p>');
        echo('<div class="alert alert-danger"><strong>Server ikke tilgjengelig</strong></div>');
        echo('');

      }
    }


    private function getPrinterInfo() {
      echo('<p><strong>'.Settings::PRINTER_NAME.'</strong> <span class="label label-success pull-right">USB/SERIAL</span></p>');
      echo('<div class="alert alert-info"><strong>Manual test only</strong>');

    }






}
?>
