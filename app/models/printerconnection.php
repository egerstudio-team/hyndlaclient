<?php

class PrinterConnection {

  function __construct($type,$host,$port = NULL,$device) {
    $this->printerType = $type;
    $this->printerHost = $host;
    $this->printerPort = $port;
    $this->printerDevice = $device;

    switch($this->printerType) {

      case 'IP':
      $this->printerConnector = 'NetworkPrintConnector';
      $connector = new $this->printerConnector($this->printerHost,$this->printerPort);
      break;

      case 'USB':
      $this->printerConnector = 'FilePrintConnector';
      $connector = new $this->printerConnector($this->printerDevice);
      break;

    }



    $this->printer = new Escpos($connector);

  }




}

/* A wrapper to do organise item names & prices into columns */
class configItem {
	private $name;
	private $value;

	public function __construct($name = '', $value = '') {
		$this -> name = $name;
		$this -> value = $value;
	}

	public function __toString() {
		$rightCols = 20;
		$leftCols = 22;
		$left = str_pad($this -> name, $leftCols) ;
		$right = str_pad($this -> value, $rightCols, ' ', STR_PAD_LEFT);
		return "$left$right\n";
	}
}
