<?php

// error logging
require_once(dirname(__FILE__) . '/error/errorhandler.php');
require_once(dirname(__FILE__) . '/log/loghandler.php');


//boot file
require_once(dirname(__FILE__) . '/../config/settings.php');
require_once(dirname(__FILE__) . '/models/status.php');
require_once(dirname(__FILE__) . '/models/printerconnection.php');
