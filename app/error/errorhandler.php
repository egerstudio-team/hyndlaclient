<?php

error_reporting(E_ALL);

function hyndlaErrorHandler($error_no,$error_msg)
{
  error_log("HyndlaError: ".$error_no." : ".$error_msg );
}


set_error_handler('hyndlaErrorHandler');

ini_set("log_errors", 1);
ini_set("error_log", dirname(__FILE__) . "/../../log/errors.log");
