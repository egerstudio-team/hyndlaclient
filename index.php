<?php
require_once('include/header.php') ?>

  <body role="document">

    <?php require_once('include/navigation.php'); ?>

    <div class="container theme-showcase" role="main">

      <!-- Main jumbotron for a primary marketing message or call to action -->
      <div class="jumbotron system-status">
        <img src="assets/img/hyndla_logo_web_small.png" class="img-responsive center-block">
        <p>HyndlaClient fra Eger Studio tilbyr utskriftstjenester lokalt fra nettbasert ordresystem</p>
      </div>


      <div class="page-header">
        <h1>System status</h1>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">PRINTER</h3>
            </div>
            <div class="panel-body">
              <div id="printer">
                <p class="lead text-center"><i class="fa fa-refresh fa-spin"></i> laster inn...</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">VPN SERVER</h3>
            </div>
            <div class="panel-body">
              <div id="vpn">
                <p class="lead text-center"><i class="fa fa-refresh fa-spin"></i> laster inn...</p>
              </div>
            </div>
          </div>
        </div>

        <div class="col-sm-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title">SYSTEM SERVER</h3>
            </div>
            <div class="panel-body">
              <div id="appserver">
                <p class="lead text-center"><i class="fa fa-refresh fa-spin"></i> laster inn...</p>
              </div>
            </div>
          </div>
        </div>

        </div>
        <!-- end row -->
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Alternativer</h3>
              </div>
              <div class="panel-body">
                <button type="button" class="btn btn-default" id="systemStatusOff"><i class="fa fa-exclamation"></i> Slå av systemstatus</button>
                <button type="button" class="btn btn-default" id="systemStatusOn"><i class="fa fa-caret-right"></i> Slå på systemstatus</button>
                <button type="button" class="btn btn-default pull-right" id="printSystemConfig"><i class="fa fa-print"></i> Skriv ut konfigurasjon</button>
              </div>
            </div>
          </div>
        </div>
        <!-- end row -->
        <?php require_once('include/bottom.php'); ?>


    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
    <script src="assets/js/bootSystemStatus.js"></script>



  </body>
</html>
